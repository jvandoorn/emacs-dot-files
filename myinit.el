(setq gc-cons-threshold 100000000) ;; Garbage collection, prevents hangs
(setq inhibit-startup-screen 1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "<f5>") 'revert-buffer)
(require 'recentf)
(recentf-mode 1) ;; Remember recently opened files
(auto-fill-mode -1)
(remove-hook 'text-mode-hook #'turn-on-auto-fill)
(global-visual-line-mode 1) ;; Wrap lines (instead of fill)
(blink-cursor-mode -1) ;; Don't blink the cursor
(show-paren-mode 1) ;; Highlight matching delimiters
(global-hl-line-mode t) ;; Highlight pointer line

(setq-default frame-title-format '("%b [%m]")) ;;Changes the frame name

(setq org-src-window-setup 'split-window-below) ;;Changes where the new window opens for C-c '

;; Backup Control
(setq backup-directory-alist `(("." . "~/.emacs.d/autosaves/"))) ;;Makes a dir for auto-saves to reduce clutter
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t
      backup-by-copying t)

;; Server
(require 'server)
(or (server-running-p)
    (server-start))

;; Hide mode line
(use-package hide-mode-line
  :ensure t
  :config
  (add-hook 'completion-list-mode-hook #'hide-mode-line-mode))

;; Chicago, IL (check with M-x sunrise-sunset)
(setq calendar-latitude 41.869)
(setq calendar-longitude -87.671)
(setq calendar-location-name "Chicago, IL")

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox-light-soft))

;; (use-package circadian
;;   :ensure t
;;   :config
;;   ;; Set theme for times
;;   (setq circadian-themes '((:sunrise . gruvbox-light-medium)
;;                            (:sunset . gruvbox)))
;;   (circadian-setup)
;;   (add-hook 'circadian-after-load-theme-hook
;;             #'(org-bullets-mode 1)))

(use-package ivy
  :ensure t
  :demand t
  :diminish ivy-mode
  :bind (("C-x b" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t
        ivy-display-style 'fancy
        ivy-count-format "%d/%d "))

(use-package counsel
  :ensure t
  :diminish counsel-mode
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
         ("C-r" . swiper)
         ("C-c C-r" . ivy-resume)
         ("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file))
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))

(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (require 'smartparens-config)
  (smartparens-global-mode 1))

(add-hook 'prog-mode-hook 'linum-mode t)

(use-package rainbow-delimiters
  :ensure t
  :diminish rainbow-delimiters-mode
  :config (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package pdf-tools
  :pin manual
  :ensure t
  :demand t
  :bind (("M-s o" . pdf-occur))
  :config
  (pdf-tools-install) ;It needs to initialize
  (setq-default pdf-view-display-size 'fit-width)
  (require 'pdf-occur)
  ;(setq pdf-annot-activate-created-annotations t)
  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward) ;Swiper doesn't work here
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
        TeX-source-correlate-start-server t)

  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)

  (add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1))))

(use-package pdf-view-restore
  :after pdf-tools
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode))

(require 'org)

(custom-set-variables
 '(org-directory "~/Documents/orgfiles")
 '(org-default-notes-file (concat org-directory "/i.org"))
 '(org-export-html-postamble nil)
 '(org-hide-leading-stars t)
 '(org-startup-folded (quote overview))
 '(org-startup-indented t)
 )

(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cl" 'org-store-link)

;; pretty src blocks
(setq org-src-fontify-natively t)

(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

(use-package org-pomodoro
  :ensure t
  :config
  ;(global-set-key "\C-cp" 'org-pomodoro)
  (setq org-pomodoro-length 20) ;; 20 minute pomodoros
  (setq org-pomodoro-short-break-length 4)) ;; 4 minute breaks

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "IN-PROGRESS(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELED(c@/!)" "MEETING"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("IN-PROGRESS" :foreground "cyan" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold))))

(setq org-priority-faces '((?A . (:foreground "red" :weight bold))
                           (?B . (:foreground "OrangeRed1"))
                           (?C . (:foreground "goldenrod"))
                           (?1 . (:foreground "gold3" :weight bold)) ;Gold
                           (?2 . (:foreground "azure3" :weight bold)) ;Silver
                           (?3 . (:foreground "chocolate" :weight bold)) ;Bronze
                           (?4 . (:foreground "bisque4")) ;Iron
                           (?5 . (:foreground "SlateGrey" :slant italic)))) ;Stone

(defun mark-done-and-archive ()
  "Mark the state of an org-mode item as DONE and archive it."
  (interactive)
  (org-todo 'done)
  (org-archive-subtree))

(define-key org-mode-map (kbd "C-c C-x C-s") 'mark-done-and-archive)

(setq org-log-done 'time)

;(use-package org-ac
 ; :ensure t
 ; :init (progn
 ;         (require 'org-ac)
 ;         (org-ac/config-default)
 ;         ))

(global-set-key (kbd "C-c c") 'org-capture)

(setq org-capture-templates
      '(
        ;; Todo Tasks
        ("t" "Todo")
        ("tm" "Meditation task" entry (file+headline "~/Research/lab.org" "Meditation tasks")
         "* TODO %?\n%U" :prepend t)
        ("ts" "SGB task" entry (file+headline "~/Research/lab.org" "SGB tasks")
         "* TODO %?\n%U" :prepend t)
        ("ti" "IMPACT task" entry (file+headline "~/Research/lab.org" "IMPACT tasks")
         "* TODO %?\n%U" :prepend t)
        ("tb" "MsBrain task" entry (file+headline "~/Research/lab.org" "MsBrain tasks")
         "* TODO %?\n%U" :prepend t)
        ("tg" "Grad school task" entry (file+headline "~/Research/lab.org" "Grad school tasks")
         "* TODO %?\n%U" :prepend t)
        ("th" "HCP task" entry (file+headline "~/Research/lab.org" "HCP tasks")
         "* TODO %?\n%U" :prepend t)

        ;; Lab Meeting
        ("m" "Lab Meeting")
        ("mc" "CoNECt Meeting" entry (file+headline "~/Research/lab.org" "CoNECt Meetings")
         "* MEETING with CoNECT Lab (%^{Who is presenting?} presenting) %U \n %?"
         :prepend t :clock-in t :clock-resume t)
        ("mw" "WMHRG Meeting" entry (file+headline "~/Research/lab.org" "WMHRG Meetings")
         "* MEETING with Maki Group (%^{Purpose?}) %U \n %?" :prepend t :clock-in t :clock-resume t)
        ("mo" "Other Meeting" entry (file+headline "~/Research/lab.org" "Other Meetings")
         "* MEETING with %^{With whom?} %U \n %?" :prepend t :clock-in t :clock-resume t)

        ;; Note items
        ("n" "Quick Note" entry (file+headline "~/Documents/orgfiles/i.org" "Notes")
         "* %?\n%U")
        ("f" "Fortune" plain (file "/usr/share/games/fortunes/jvd") "%?\n%")

       ))

(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-tags-exclude-from-inheritance (quote ("crypt")))
(setq org-crypt-key nil)
(setq auto-save-default t)
;; Auto-saving does not cooperate with org-crypt.el: so you need
;; to turn it off if you plan to use org-crypt.el quite often.
;; Otherwise, you'll get an (annoying) message each time you
;; start Org.
;; To turn it off only locally, you can insert this:
;;
;; # -*- buffer-auto-save-file-name: nil; -*-

(use-package org-pdftools
  :ensure t
  :after org
  :config
  (add-to-list 'org-file-apps
               '("\\.pdf\\'" . (lambda (file link)
                                 (org-pdftools-open link)))))

(require 'org-ref)
(require 'org-ref-wos)
(require 'org-ref-pubmed)
(require 'doi-utils)
(require 'org-ref-arxiv)
(require 'org-ref-latex)

(setq reftex-default-bibliography '("~/Research/work-comp.bib"))

(setq org-ref-notes-directory "~/Documents/orgfiles/notes/")
(setq org-ref-bibliography-notes "~/Documents/orgfiles/articles.org")
(setq org-ref-default-bibliography '("~/.emacs.d/pubmed.bib"))
(setq org-ref-pdf-directory "~/Documents/orgfiles/storage/")

(setq helm-bibtex-bibliography '("~/Documents/orgfiles/library.bib"
                                 "~/Research/work-comp.bib"))
(setq helm-bibtex-notes-path "~/Documents/orgfiles/articles.org")

;; Set hook to nil to allow for custom org-ref-note-title-format
(setq org-ref-create-notes-hook nil)

(defun my/org-ref-open-pdf-at-point ()
  "Open the pdf for bibtex key under point if it exists, Documents/orgfiles version."
  (interactive)
  (let* ((results (org-ref-get-bibtex-key-and-file))
         (key (car results))
     (pdf-file (car (bibtex-completion-find-pdf key))))
    (if (file-exists-p pdf-file)
    (org-open-file pdf-file)
      (message "No PDF found for %s" key))))

(setq org-ref-open-pdf-function 'my/org-ref-open-pdf-at-point)

(setq bibtex-completion-pdf-field "file")
(setq org-ref-get-pdf-filename-function
      'org-ref-get-pdf-filename-helm-bibtex)

  ;; Custom org-ref-note-title-format for interleaving pdf
(setq org-ref-note-title-format
"** TODO %y - %t cite:%k
:PROPERTIES:
:Custom_ID: %k
:AUTHOR: %9a
:JOURNAL: %j
:YEAR: %m, %y
:DOI: %D
:END:

")

(defun my/org-ref-notes-function (candidates)
    (let ((key (helm-marked-candidates)))
      (funcall org-ref-notes-function (car key))))

(helm-delete-action-from-source "Edit notes" helm-source-bibtex)
(helm-add-action-to-source "Edit notes" 'my/org-ref-notes-function helm-source-bibtex 7)

(setq org-agenda-include-diary t)

(setq package-check-signature nil)

(setq org-agenda-custom-commands
  '())

(setq org-agenda-custom-commands
      '(("n" "Agenda and TODO" ((agenda "") (tags-todo "-Gaming")))
        ("P" "Past-due" ((tags "TIMESTAMP<=\"<now>\"")))))

;; Set agenda files
(setq org-agenda-files (list "~/Documents/orgfiles"
                                 "~/Research/meditation_paper/med_paper.org"
                                 "~/Research/lab.org"
                                 "~/Research/SGB/sgb.org"
                                 "~/Research/Dissertation/phd.org"))

(add-to-list 'org-structure-template-alist '("p" .  "proof"))
(add-to-list 'org-structure-template-alist '("b" .  "abstract"))

(use-package org2blog
  :ensure t
  :defer t
  :config
  ;; Basic setup
  (setq org2blog/wp-blog-alist
        '(("professional"
           :url "https://jlvandoorn.wordpress.com/xmlrpc.php"
           :username "jlvandoorn")))

  ;; Let Org2blog work from any org buffer with #+ORG2BLOG in the header
  (add-hook 'org-mode-hook #'org2blog-maybe-start)

  ;; Make it easy to open Org2blog from anywhere
  (global-set-key (kbd "C-c b") #'org2blog-user-interface)

  ;; Automatically upload image links
  (setq org2blog/wp-image-upload t))

;; Ensure ipython (note, ob-ipython is abandoned)
(use-package jupyter
  :ensure t)

;; Load languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (R . t)
   (emacs-lisp . t)
   (latex . t)
   (matlab . t)
   (js . t)
   (octave . t)
   (lisp . t)
   (ditaa . t)
   (gnuplot . t)
   ))


;;; Python command for org-babel
(setq org-babel-python-command "/usr/bin/python3")

(use-package org-chef
  :ensure t)

(use-package org-drill
  :ensure t
  :config (progn
            (add-to-list 'org-modules 'org-drill)
            (setq org-drill-add-random-noise-to-intervals-p t)
            (setq org-drill-hint-separator "||")
            (setq org-drill-left-cloze-delimiter "<[")
            (setq org-drill-right-cloze-delimiter "]>")
            (setq org-drill-use-visible-cloze-face-p t)
            (setq org-drill-hide-item-headings-p t)
            (setq org-drill-maximum-items-per-session 40)
            (setq org-drill-maximum-duration 30)   ; 30 minutes
            (setq org-drill-save-buffers-after-drill-sessions-p nil)
            (setq org-drill-learn-fraction 0.25)))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config (which-key-mode 1))

(use-package company
  :ensure t
  :diminish company-mode
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-show-numbers t))

(use-package smooth-scrolling
  :ensure t
  :config
  (smooth-scrolling-mode 1)
  (setq smooth-scroll-margin 5))

(use-package writeroom-mode
  :ensure t
  :config
  (global-set-key (kbd "C-c w") 'writeroom-mode))

(require 'desktop)
(setq desktop-save 1
      desktop-load-locked-desktop t
      desktop-restore-frames nil
      ;; Don't save remote files and/or *gpg files.
      desktop-files-not-to-save "\\(^/[^/:]*:\\|(ftp)$\\)\\|\\(\\.gpg$\\)")
(desktop-save-mode 1)

;; Use mouse forward and backward to navigate buffers
(global-set-key (kbd "<mouse-8>") 'previous-buffer)  ; back button
(global-set-key (kbd "<mouse-9>") 'next-buffer)  ; forward button

;; Except in PDF mode
(define-key pdf-view-mode-map (kbd "<mouse-8>") 'pdf-view-next-page)
(define-key pdf-view-mode-map (kbd "<mouse-9>") 'pdf-view-previous-page)

(setq ediff-split-window-function 'split-window-horizontally)

(use-package paradox
  :ensure t
  :config
  (paradox-enable))

(use-package eyebrowse
  :ensure t
  :custom (eyebrowse-keymap-prefix (kbd "C-x w"))
  :diminish eyebrowse-mode
  :config (progn
            (define-key eyebrowse-mode-map (kbd "C-c 1") 'eyebrowse-switch-to-window-config-1)
            (define-key eyebrowse-mode-map (kbd "C-c 2") 'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "C-c 3") 'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "C-c 4") 'eyebrowse-switch-to-window-config-4)
            (define-key eyebrowse-mode-map (kbd "C-c 5") 'eyebrowse-switch-to-window-config-5)
            (define-key eyebrowse-mode-map (kbd "C-c ,") 'eyebrowse-rename-window-config)
            (eyebrowse-mode t)
            (setq eyebrowse-new-workspace t)
            (setq eyebrowse-mode-line-separator " ")))

(autoload 'ibuffer "ibuffer" "List buffers." t)
(global-set-key (kbd "C-x C-b") 'ibuffer)

(use-package avy
  :ensure t
  :bind ("C-:" . avy-goto-char-timer))

(use-package ace-link
  :ensure t
  :config
  (ace-link-setup-default)
  (define-key org-mode-map (kbd "M-o") 'ace-link-org))

(use-package ace-window
  :ensure t
  :bind (("C-c j" . ace-window))
  :config
  (ace-window-display-mode t))

(use-package undo-tree
  :ensure t
  :init (global-undo-tree-mode)
  :diminish undo-tree-mode)

(use-package neotree
  :ensure t
  :config
  (global-set-key (kbd "C-c t") 'neotree-toggle)
  (setq neo-theme 'ascii)
  (setq neo-smart-open t))

(use-package move-text
  :ensure t
  :config
  (move-text-default-bindings))

(use-package expand-region
  :ensure t
  :config
  (global-set-key (kbd "C-=") 'er/expand-region))

(setq abbrev-file-name
      "~/.emacs.d/.abbrev_defs")
(setq save-abbrevs 'silently)
(add-hook 'text-mode-hook #'abbrev-mode)

(define-key ctl-x-map "\C-i"
  #'endless/ispell-word-then-abbrev)

(defun endless/simple-get-word ()
  (car-safe (save-excursion (ispell-get-word nil))))

(defun endless/ispell-word-then-abbrev (p)
  "Call `ispell-word', then create an abbrev for it.
With prefix P, create local abbrev. Otherwise it will
be global.
If there's nothing wrong with the word at point, keep
looking for a typo until the beginning of buffer. You can
skip typos you don't want to fix with `SPC', and you can
abort completely with `C-g'."
  (interactive "P")
  (let (bef aft)
    (save-excursion
      (while (if (setq bef (endless/simple-get-word))
                 ;; Word was corrected or used quit.
                 (if (ispell-word nil 'quiet)
                     nil ; End the loop.
                   ;; Also end if we reach `bob'.
                   (not (bobp)))
               ;; If there's no word at point, keep looking
               ;; until `bob'.
               (not (bobp)))
        (backward-word)
        (backward-char))
      (setq aft (endless/simple-get-word)))
    (if (and aft bef (not (equal aft bef)))
        (let ((aft (downcase aft))
              (bef (downcase bef)))
          (define-abbrev
            (if p local-abbrev-table global-abbrev-table)
            bef aft)
          (message "\"%s\" now expands to \"%s\" %sally"
                   bef aft (if p "loc" "glob")))
      (user-error "No typo at or before point"))))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode)
  :diminish flycheck-mode
  :config
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (setq ess-use-flymake nil))

(use-package hungry-delete
  :ensure t
  :diminish hungry-delete-mode
  :config
  (global-hungry-delete-mode))

(use-package yasnippet
  :ensure t
  :init (yas-global-mode 1))

(use-package yasnippet-snippets
  :ensure t)

(use-package define-word
  :ensure t)

;; Spellchecker
(dolist (hook '(org-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

(when (executable-find "hunspell")
  (setq-default ispell-program-name "hunspell")
  (setq ispell-really-hunspell t))
(eval-after-load "flyspell"
  '(progn
     (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
     (define-key flyspell-mouse-map [mouse-3] #'undefined)))

;; easy spell check
(global-set-key (kbd "C-S-<f8>") 'flyspell-mode)
(global-set-key (kbd "C-c s") 'flyspell-buffer)
(defun flyspell-check-next-highlighted-word ()
  "Custom function to spell check next highlighted word"
  (interactive)
  (flyspell-goto-next-error)
  (ispell-word)
  )
;;(global-set-key (kbd "C-c n") 'flyspell-check-next-highlighted-word)

(defun jvd-save-word ()
  "Save a word to dictionary quickly."
  (interactive)
  (let ((current-location (point))
        (word (flyspell-get-word)))
    (when (consp word)
      (flyspell-do-correct 'save nil (car word) current-location (cadr word) (caddr word) current-location)))
  ;(flyspell-buffer)
  )

(global-set-key (kbd "C-c n") 'jvd-save-word)

(autoload 'gnugo "gnugo" "GNU Go" t)

(setq gnugo-option-history (list "--komi 0.5 --boardsize 19 --level 1"))
;; Can also set --level to 1-10, and switch between --japanese-rules and --chinese-rules

(setq gnugo-xpms 'gnugo-imgen-create-xpms)
;; This allows the Goban to be displayed graphically.
(add-hook 'gnugo-start-game-hook 'gnugo-image-display-mode)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq org-latex-create-formula-image-program 'imagemagick)
(setq org-latex-tables-booktabs t)

(setq org-latex-packages-alist '(("margin=2cm" "geometry" nil)
                                 ("" "booktabs" nil)
                                 ("" "minted" t)))

;; Pretty code blocks
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "lines")
        ("breaklines" "true")
        ("bgcolor" "lightgray")
        ("style" "vs")
        ("linenos=false")))


(setq  org-latex-pdf-process
   '("latexmk -shell-escape -bibtex -pdf %f"))

(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)

;; Set ipython minted same as python
(add-to-list 'org-latex-minted-langs '(ipython "python"))

;; Other packages
;; (add-to-list 'org-latex-packages-alist '("euler" "textgreek"))
;; (add-to-list 'org-latex-packages-alist '("usenames,dvipsnames,svgnames,table" "xcolor"))
;; (add-to-list 'org-latex-packages-alist '("" "microtype"))
;; (add-to-list 'org-latex-packages-alist '("" "lmodern"))
;; (add-to-list 'org-latex-packages-alist '("super,comma" "natbib"))



;; Add UIC Thesis Class and Beamer
(require 'ox-latex)
(add-to-list 'org-latex-classes
             '("thesis"
"\\documentclass[11pt,titlepage]{article}
[DEFAULT-PACKAGES]
\\usepackage{lmodern}
\\usepackage{fixltx2e}
\\usepackage{float}
\\usepackage{marvosym}
\\usepackage{wasysym}
\\tolerance=1000
[PACKAGES]
\\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\\usepackage[euler]{textgreek}
\\usepackage{microtype}
\\usepackage{setspace}
\\usepackage{lipsum}
\\usepackage[super,comma]{natbib}
\\usepackage[left=0.5in, right=0.5in, top=0.5in, bottom=0.5in]{geometry}
\\usepackage[font=normalsize,labelfont=bf]{caption}
\\usepackage{sectsty}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(require 'ox-beamer)
(add-to-list 'org-latex-classes
             '("beamer"
               "\\documentclass[presentation]{beamer}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection{%s}")))

(setq org-format-latex-options (plist-put org-format-latex-options :scale 1.2))

(use-package pubmed
    :ensure t
    :commands (pubmed-search pubmed-advanced-search pubmed-dissemin pubmed-unpaywall pubmed-scihub)
    :bind (("C-c p" . pubmed-search))
    :config
    (setq pubmed-unpaywall-email "jvando3@uic.edu")
    (setq pubmed-scihub-url "https://sci-hub.st/")
    (setq pubmed-fulltext-functions '(pubmed-pmc
                                      pubmed-openaccessbutton
                                      pubmed-dissemin
                                      pubmed-scihub
                                      pubmed-unpaywall))
    (setq pubmed-bibtex-keypattern "[auth:lower]_[veryshorttitle]_[year]")
    (setq pubmed-max-results 100)
    (setq pubmed-sort-method 'relevance)
    (setq pubmed-bibtex-apply-clean nil)
(setq pubmed-bibtex-default-file "/home/jvandoorn/Research/work-comp.bib"))

(require 'reftex-cite)
(setq bibtex-completion-bibliography '("~/Research/work-comp.bib")
      bibtex-completion-notes-path "~/Documents/orgfiles/articles.org")

(setq bibtex-completion-pdf-field "file")


(global-set-key (kbd "C-c h") 'helm-bibtex)

(use-package ess-r-mode)
(global-set-key (kbd "C-c r") 'ess-request-a-process) ;switch R processes

(ess-set-style 'RStudio)
(setq ess-offset-arguments 'prev-line)

;;; Hook some useful programming minor modes
(add-hook 'ess-mode-hook #'rainbow-delimiters-mode)
(add-hook 'ess-mode-hook #'nlinum-mode)

(setq ess-use-auto-complete nil)

;;; Configure assignment key to ";" instead of "_"
;;; (Press ";" again to get the semicolon symbol)
(define-key ess-r-mode-map ";" #'ess-insert-assign)
(define-key inferior-ess-r-mode-map ";" #'ess-insert-assign)

(require 'ansi-color)
(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))
(global-set-key (kbd "C-c d") 'display-ansi-colors)

(use-package slime
  :ensure t
  :config
  (setq inferior-lisp-program "/usr/bin/clisp")
  (add-hook 'slime-mode-hook
          (lambda ()
            (unless (slime-connected-p)
              (save-excursion (slime)))))
  (add-hook 'slime-repl-mode-hook (lambda ()
                                  (rainbow-delimiters-mode t)
                                  (smartparens-strict-mode t)
                                  (set-up-slime-ac)
                                  (auto-complete-mode))))

(use-package ac-slime
  :ensure t)

(use-package eval-sexp-fu
  :ensure t)

;; Path stuff
(setenv "PATH" (concat "/usr/bin/python3" (getenv "PATH")))
(setenv "PATH" (concat "/usr/bin/python3" (getenv "PATH")))

(setq exec-path (split-string (getenv "PATH") path-separator))
(setq realgud:pdb-command-name "python3 -m pdb")

;; Enable elpy
(elpy-enable)

;; Enable Flycheck
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; Python documentation
;(add-to-list 'load-path "~/path/to/pydoc-info")
;(require 'pydoc-info)

;; Use python3 with ipython
(setq elpy-rpc-python-command "/usr/bin/python3")
(setq elpy-syntax-check-command "/usr/bin/flake8")
(setq python-shell-interpreter "/usr/bin/ipython"
      python-shell-interpreter-args "-i --simple-prompt --pprint")

;; PDB
(setq gud-pdb-command-name "python3 -m pdb")

;; Environment set up (from ipython.org documentation)
(defvar server-buffer-clients)
(when (and (fboundp 'server-start) (string-equal (getenv "TERM") 'xterm))
  (server-start)
  (defun fp-kill-server-with-buffer-routine ()
    (and server-buffer-clients (server-done)))
  (add-hook 'kill-buffer-hook 'fp-kill-server-with-buffer-routine))


(add-hook 'before-save-hook
          (lambda ()
            (when 'elpy-mode
              (delete-trailing-whitespace))))

;; Make tabs and spaces consistent
(add-hook 'elpy-mode-hook
          (lambda ()
            (setq-default indent-tabs-mode nil)
            (setq-default tab-width 4)
            (setq-default python-indent 4)))

 ;; Anaconda3
(use-package conda
  :ensure t
  :config
  (conda-env-initialize-eshell))

(setq python-shell-prompt-detect-failure-warning nil)

(with-eval-after-load 'ipython
  (defun python-shell-completion-native-try ()
    "Return non-nil if can trigger native completion."
    (let ((python-shell-completion-native-enable t)
          (python-shell-completion-native-output-timeout
           python-shell-completion-native-try-output-timeout))
      (python-shell-completion-native-get-completions
       (get-buffer-process (current-buffer))
       nil "_"))))

(global-set-key (kbd "M-g a") "α")
(global-set-key (kbd "M-g b") "β")
(global-set-key (kbd "M-g g") "γ")
(global-set-key (kbd "M-g d") "δ")
(global-set-key (kbd "M-g e") "ε")
(global-set-key (kbd "M-g z") "ζ")
(global-set-key (kbd "M-g h") "η")
(global-set-key (kbd "M-g q") "θ")
(global-set-key (kbd "M-g i") "ι")
(global-set-key (kbd "M-g k") "κ")
(global-set-key (kbd "M-g l") "λ")
(global-set-key (kbd "M-g m") "μ")
(global-set-key (kbd "M-g n") "ν")
(global-set-key (kbd "M-g x") "ξ")
(global-set-key (kbd "M-g o") "ο")
(global-set-key (kbd "M-g p") "π")
(global-set-key (kbd "M-g r") "ρ")
(global-set-key (kbd "M-g s") "σ")
(global-set-key (kbd "M-g t") "τ")
(global-set-key (kbd "M-g u") "υ")
(global-set-key (kbd "M-g f") "ϕ")
(global-set-key (kbd "M-g j") "φ")
(global-set-key (kbd "M-g c") "χ")
(global-set-key (kbd "M-g y") "ψ")
(global-set-key (kbd "M-g w") "ω")
(global-set-key (kbd "M-g A") "Α")
(global-set-key (kbd "M-g B") "Β")
(global-set-key (kbd "M-g G") "Γ")
(global-set-key (kbd "M-g D") "Δ")
(global-set-key (kbd "M-g E") "Ε")
(global-set-key (kbd "M-g Z") "Ζ")
(global-set-key (kbd "M-g H") "Η")
(global-set-key (kbd "M-g Q") "Θ")
(global-set-key (kbd "M-g I") "Ι")
(global-set-key (kbd "M-g K") "Κ")
(global-set-key (kbd "M-g L") "Λ")
(global-set-key (kbd "M-g M") "Μ")
(global-set-key (kbd "M-g N") "Ν")
(global-set-key (kbd "M-g X") "Ξ")
(global-set-key (kbd "M-g O") "Ο")
(global-set-key (kbd "M-g P") "Π")
(global-set-key (kbd "M-g R") "Ρ")
(global-set-key (kbd "M-g S") "Σ")
(global-set-key (kbd "M-g T") "Τ")
(global-set-key (kbd "M-g U") "Υ")
(global-set-key (kbd "M-g F") "Φ")
(global-set-key (kbd "M-g J") "Φ")
(global-set-key (kbd "M-g C") "Χ")
(global-set-key (kbd "M-g Y") "Ψ")
(global-set-key (kbd "M-g W") "Ω")

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))

(use-package csv
  :ensure t
  :mode (".tsv" ".csv")
  :custom (csv-comment-start "##")
  :hook
  (csv-mode . (lambda ()
                  (csv-align-mode t)
                  (toggle-truncate-lines 1)
                  (csv-header-line t))))

(use-package nov
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

(use-package markdown-mode
    :ensure t
    :config
    (add-to-list 'auto-mode-alist '("\\.Rmd\\'" . markdown-mode)))

(use-package engine-mode
  :ensure t
  :config
  (engine/set-keymap-prefix (kbd "C-c g"))
  (engine-mode t)
  (defengine wikipedia
    "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"
    :keybinding "w")
  (defengine duckduckgo
    "https://duckduckgo.com/?q=%s"
    :keybinding "d")
  (defengine google
    "http://www.google.com/search?ie=utf-8&oe=utf-8&q=%s"
    :keybinding "g")
  (defengine stack-overflow
    "https://stackoverflow.com/search?q=%s"
    :keybinding "s"
    :docstring "Consult the sages"))

(use-package elfeed
  :ensure t
  :config
  (global-set-key (kbd "C-c e") 'elfeed)
  (setq-default elfeed-search-filter "@1-week-ago +unread")
  (setq elfeed-feeds
        '(("https://xkcd.com/rss.xml" comics)
          ("https://www.smbc-comics.com/comic/rss" comics)
          ("https://www.motherjones.com/feed/" news)
          ("https://anarchism.pageabode.com/rss.xml" anarchy)
          ("https://anarchistnews.org/rss.xml" anarchy)
          ("http://blog.linuxmint.com/?feed=rss2" linux)
          ("https://static.fsf.org/fsforg/rss/news.xml" news)
          ("https://vegnews.com/news/feed.rss" vegan))))

(require 'magit)

(global-set-key (kbd "C-x g") 'magit-status)

(use-package delight
  :ensure t
  :config
  (delight '((abbrev-mode nil abbrev)
             (auto-revert-mode nil autorevert)
             (flyspell-mode nil flyspell)
             (org-indent-mode nil org-indent)
             (visual-line-mode nil emacs))))

(defun jvd-dired-copy-path-at-point ()
    "Yank absolute path to file at point in Dired"
  (interactive)
  (dired-copy-filename-as-kill 0))

(define-key dired-mode-map (kbd "W") 'jvd-dired-copy-path-at-point)

(defun info-elisp-manual ()
  "Display the Elisp manual in Info mode."
  (interactive)
  (info "elisp"))

(global-set-key (kbd "C-h E") 'info-elisp-manual)

(defun markdown-convert-buffer-to-org ()
   "Convert the current buffer's content from markdown to orgmode format and save it with the current buffer's file name but with .org extension."
   (interactive)
   (shell-command-on-region (point-min) (point-max)
                            (format "pandoc -f markdown -t org -o %s"
                                    (concat (file-name-sans-extension (buffer-file-name)) ".org"))))

(random t) ;Set new seed on init.

(defun roll-d100 ()
  "Insert a random number in [0,100)."
  (interactive)
  (insert (number-to-string (random 100))))

;(global-set-key (kbd "C-c R") 'jvd-roll-d100)

(defun roll-desc (DESC)
  "Insert a random number in the interval [0,100) generated from a string seed."
  (interactive)
  (insert (number-to-string (% (random DESC) 100))))

;; put fortune in scratch buffer
(setq initial-scratch-message
      (format
       ";; %s\n\n"
       (replace-regexp-in-string
        "\n" "\n;; " ; comment each line
        (replace-regexp-in-string
         "\n$" ""    ; remove trailing linebreak
         (shell-command-to-string "fortune")))))
